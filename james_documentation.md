##**Documentation for PC Optimizer related things**

Normal PC Optimization workflow:

1. Authenticate with LKQD and get a valid session cookie.
2. Get all supply tag ids.
3. Get domain / app list ids for each tag if they have one. If a Whitelist is attached, do not optimize that tag.
4. Pull either a domain or app name performance report for yesterday's data for that tag.
5. Get all domains/apps that don't meet the minimum criteria (>2000 opps, <0.10% fill per app/domain).
6. If that supply tag has a list already, add them to the existing domain/app list id. Otherwise create a new domain/app list and attach it to the supply tag.


<br/><br/>

### Base url for LKQD api calls: https://ui-api.lkqd.com

<br/>

####1: Getting LKQD-Api-Version header, and authenticating successfully:
#####Endpoint (for auth): `POST /sessions`
LKQD-Api-Version header is always changing which is why I figure that out first before making any future calls.
#####My Code:
```python
    def get_auth(self, username, password):
        """
        Initial authentication function. Uses the new Session
        and successfully authenticates it. Saves as self.Session for later use.
        :return: None | Saves everything in current self.Session object
        """
        self.type_assert(locals(), d={"username": str, "password": str})
        # set user-agent to something other than requests
        self.Session.headers.update({"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36"})
        # get the current lkqd api version
        r = self.Session.get("https://ui.lkqd.com/login")
        if r.ok:
            # rough text splicing to find the current api version Todo// Cleaner way??
            self.Session.headers.update({"LKQD-Api-Version": r.content[r.content.find("GLOBAL_API_VERSION"):].split(',')[0].split('=')[-1].replace(' ', '').replace("'", "")})
        else:
            raise AuthenticationError("Could not get current LKQD Api Version: {c}:{e}".format(c=r.status_code, e=r.content))

        # authenticate the current session with lkqd, uses Session.post instead of self.post to keep headers
        rr = self.Session.post(self.BASE_URL + '/sessions', json={"login": username, "password": password})
        if not rr.ok:
            raise AuthenticationError("Could not authenticate with LKQD: {c}: {e}".format(c=rr.status_code, e=rr.content))
```

<br/>

####2: Get all supply platform connection tag ids:
#####Endpoint: `GET /supply/listing-rows`

When calling this endpoint it will only retrieve basic info about each tag (as seen below). It does not contain any information such as domain/app lists.
#####Supply Tag "Overview" Object Response:
```json
{  
   "status":           string,
   "environmentId":    int,
   "cpmFloorDemand":   float,
   "costType":         string,
   "siteName":         string,
   "siteType":         string,
   "cpmFloor":         float,
   "cost":             float,
   "partnerId":        int,
   "partnerName":      string,
   "siteId":           int,
   "hasSspSampleUrls": bool
}
```

#####My Code:
```python
def getpctags(client):
    """
    Retrieves all PC tags from LKQD as a list.
    :param client: obj  | LKQD Client()
    :return:       list | list of all PC tags (as dicts), ex: [{"name": "Tag Name", "id": tagid}, etc, etc]
    """
    return [{"name": x['siteName'], "id": x['siteId'], "list_type": {1: "domain", 2: "app", 3: "domain", 4: "app"}[x['environmentId']], "list_id": None} for x in client.get('/supply/listing-rows')['data'] if x['status'] == 'active' and '_pc__' in x['siteName'] or '_PC_' in x['siteName']]
```
<br/>

####3: Get domain/app list id for each tag id (if applicable):
#####Endpoint: `GET /sites/{supply_id}`

This step gets the more detailed info on each supply tag such as the list ids if the tag has one, also checks if a whitelist is attached, if so don't optimize that tag id.

#####Supply Tag "Detailed" Object Response:
```json
{  
   "partnerName":               string,
   "domain":                    string,
   "publisherName":             string,
   "siteName":                  string,
   "siteType":                  string,
   "cpmFloor":                  float,
   "dynamicFloors":             int,
   "vastEnabledDesktop":        bool,
   "cost":                      float,
   "partnerId":                 int,
   "vastVpaidEnabledMobileApp": bool,
   "pmpDynamicPricingAllowed":  bool,

   // Important bit //
   "appliedRestrictoListsData": {  
      "bundleIdListId":                    int,
      "bundleIdListType":                  string,
      "domainListApplyDetected":           int,
      "appListAllowUnknownApps":           int,
      "appListType":                       string, // If tag is InApp and this is "white" don't optimize
      "appListId":                         int     // If tag is InApp and has a list, this will have an existing list id
      "deviceIdListAllowUnknownDeviceIds": int,
      "bundleIdListAllowUnknownBundleIds": int,
      "domainListBlockDetectedMismatch":   int,
      "domainListAllowUnknownDomains":     int,
      "domainListType":                    string, // if tag is Desktop/MW and this is "white" don't optimize
      "domainListId":                      int,    // If tag is Desktop/MW and has a list, this will have an existing list id    
      "bundleIdListBlockDetectedMismatch": int,
      "bundleIdListName":                  string,
      "ipListAllowUnknownIps":0
   },

   "verificationVendors":           array,
   "vastEnabledMobileApp":          bool,
   "hasSspSampleUrls":              bool,
   "status":                        string,
   "environmentId":                 int,
   "vastVpaidEnabledMobileWeb":     bool,
   "cpmFloorDemand":                float,
   "relationship":                  string,
   "vastVpaidEnabledDesktop":       bool,
   "considerSiteCostInEligibility": int,
   "siteId":                        int,
   "vastEnabledMobileWeb":          int,
   "considerFeesInEligibility":     int,
   "costType":                      string,
   "enforcePlatformConnections":    int,
   "demandTargetingType":           string,
   "prebidFilters": {  
      "whiteOps": {  
         "vendorId":          int,
         "all":               bool,
         "mobileApp":         bool,
         "ctv":               bool,
         "desktop":           bool,
         "environmentChoice": string,
         "mobileWeb":         bool
      }
   },
   "serverSideVpaidAllowMediafiles": string,
   "publisherId":                    int
}

```

#####My Code:
```python
def get_supply_details(client, tags):
    """
    Get details of all lists attached to tags. If a tag has a whitelist attached it will be deleted from the returned list.
    :param tags: list | list of all pc tags from getpctags()
    :return:     list | list of tags but with the {"list_id": id} filled in if that tag has a list
    """
    out = []
    for t in tags:
        r = client.get("/sites/{id}".format(id=t['id']))['data']
        try:
            if r['appliedRestrictoListsData']['{type}ListType'.format(type=t['list_type'])] != 'white':
                t['list_id'] = r['appliedRestrictoListsData']['{type}ListId'.format(type=t['list_type'])]
                out.append(t)
        except KeyError:
            out.append(t)
    return out
```

<br/>

####4, 5: Pull performace report, create blacklists.
Skipping this part as you guys already are doing this through the LKQD api.

<br/><br/>

###6: Applying created lists to tags (or creating a list if needed):

<br/>

####First Scenario: Supply tag has a list already attached:
####1: Get the existing domains for that list. (so not to overwrite existing)
#####Endpoint: `GET /{list_type}-lists/{list_id}`
Retrieve the domains/apps from the existing list and append to current BL to add. You will want to keep all the information returned from this as it's needed to update it after.

#####Restriction List Object Response:
```json
{  
   "description": string,
   "entries":     array,      // ex ['cnn.com', 'blank.com', 'blah.com']
   "id":          int,
   "listType":    "app-only", // only shows up for app lists, domain lists won't have this
   "name":        string,
   "publisherId": int
}
```

#####My Code Example:
```python
a = {'id': 433359, 'list_id': 229542, 'list_type': 'domain', 'name': u'S_LKQD_RocoMedia_PC_6.5_Desktop_M/L_ROW_RON_17Aug2017_N2'}
current_list = client.get("/{lt}-lists/{li}".format(lt=a['list_type'], li=a['list_id']))

new_blacklist = ['generated.com', 'from.com', 'lkqd.com', 'api.com']

[current_list['entries'].append(x) for x in new_blacklist]
```

After getting this information and adding the new domains/apps, post update to LKQD.

####2: Post update to list.

#####Endpoint: `POST /{list_type}-lists`

#####Payload:
Name and ID must match from previous GET request, and listType must be added for app-lists. Description which is changed to "Blacklist updated with new domains at {time}" or something like that.
```json
{  
   "description": string,
   "entries":     array,      // ex ['cnn.com', 'blank.com', 'blah.com', 'plus_new_domains'.com]
   "id":          int,
   "listType":    "app-only", // only if app-list, otherwise don't include listType
   "name":        string,
   "publisherId": int
}
```
#####Response:
```json
{  
   "errors": array, // list of error decriptions if any
   "status": string // "success" or "error"
}
```
<br/><br/>

####Second Scenario: Supply tag doesn't have existing list.

#####Endpoint: `POST /{list_type}-lists`

Makes sure all data is filled in from the below payload. Especially supply updates array, as this is what tells LKQD what tag to hook it up to.
You will need to have the blacklist created already before this step.
#####Payload Domain List:
```python
{
    "allowHostNames": 0,
    "dealUpdates": [],
    "description": "description here",
    "entries": [domains.com, put.com, here.com],
    "id": None,
    "name": "{current_supply_tag}-auto_list",
    "siteUpdates": [{
        "allowUnknown": 0,
        "domainListApplyDetected": 0,
        "domainListBlockDetectedMismatch": 0,
        "entityId": {current_supply_tag_id},
        "listType": "black",
        "selected": True
    }]
}
```

#####Payload App List:
```python
{  
   "allowHostNames":0,
   "dealUpdates":[],
   "description":"",
   "entries":[],
   "id":None,
   "listType": "app-only",
   "name":"{current_supply_tag_name}-auto_list",
   "siteUpdates":[{  
         "allowUnknown": 0,
         "entityId": {current_supply_tag_id},
         "listType": "black",
         "selected": True
      }]
}
```

#####Response:
```json
{
  "status": string // success or error
  "errors": array  // array of error information
}
```
